# TODO
- Ознакомиться с разметкой md, привести 10 примеров (список, заголовок, жирный шрифт, зачеркивание, разделительная горизонтальная черта, раскрасить слово цветом + 5 на свой выбор)

# Список книг
- [Идеальный программист. Как стать профессионалом разработки ПО](https://bass.netcracker.com/pages/viewpage.action?pageId=453094641)
- [Программист фанатик](https://bass.netcracker.com/pages/viewpage.action?pageId=471699619)
- [Базовые знания тестировщика веб-приложений](https://bass.netcracker.com/pages/viewpage.action?pageId=463529104)
- [Не заставляйте меня думать](https://bass.netcracker.com/pages/viewpage.action?pageId=520717801)
- [Java. Эффективное программирование](https://bass.netcracker.com/pages/viewpage.action?pageId=467209594)

# Заметки